<?php
/**
 * @copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    agape
 * @license    LGPL-3.0+
 */


class ModuleApageProperties extends BackendModule
{

    /**
     * Change the palette of the current table and switch to edit mode
     */
    public function generate()
    {
        return $this->objDc->edit($GLOBALS['AGAPE']['PROPERTIES']['ID']);
    }

    /**
     * Generate module
     */
    protected function compile()
    {
        return '';
    }
}
