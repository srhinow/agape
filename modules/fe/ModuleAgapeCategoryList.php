<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    agape
 * @license	   LGPL +3.0
 * @filesource
 */


/**
 * Class ModuleAgapeCategoryList
 */
class ModuleAgapeCategoryList extends ModuleAgape
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'agape_category_list';


    /**
     * Target pages
     * @var array
     */
    protected $arrTargets = array();


    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### AGAPE CATEGORY LIST ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        // Fallback template
        if (strlen($this->fe_agape_template)) $this->strTemplate = $this->fe_agape_template;

        return parent::generate();
    }


    /**
     * Generate module
     */
    protected function compile()
    {
        $offset = 0;
        $limit = null;
        $itemsArray = array();

        // Maximum number of items
        if ($this->fe_iao_numberOfItems > 0)
        {
            $limit = $this->fe_iao_numberOfItems;
        }

        $searchWhereArr[] = "`published` = 1";

        // Get the total number of items
        $intTotal = agape\AgapeCategoriesModel::countEntries($searchWhereArr);

        // Filter anwenden um die Gesamtanzahl zuermitteln
        if((int) $intTotal > 0)
        {
            $total = $intTotal - $offset;

            // Split the results
            if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage))
            {

                // Adjust the overall limit
                if (isset($limit))
                {
                    $total = min($limit, $total);
                }

                // Get the current page
                $id = 'page_n' . $this->id;
                $page = \Input::get($id) ?: 1;

                // Do not index or cache the page if the page number is outside the range
                if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
                {
                    global $objPage;
                    $objPage->noSearch = 1;
                    $objPage->cache = 0;

                    // Send a 404 header
                    header('HTTP/1.1 404 Not Found');
                    return;
                }

                // Set limit and offset
                $limit = $this->perPage;
                $offset = (max($page, 1) - 1) * $this->perPage;

                // Overall limit
                if ($offset + $limit > $total)
                {
                    $limit = $total - $offset;
                }

                // Add the pagination menu
                $objPagination = new Pagination($total, $this->perPage);
                $this->Template->pagination = $objPagination->generate("\n  ");
            }

            // Get the items
            if (isset($limit))
            {
                $itemsObj = agape\AgapeCategoriesModel::findCategories($limit, $offset, $searchWhereArr, array('order' => "name ASC") );
            }
            else
            {
                $itemsObj = agape\AgapeCategoriesModel::findCategories(0, $offset, $searchWhereArr, array('order' => "name ASC"));
            }

            $count = -1;

            while($itemsObj->next())
            {
                //row - Class
                $class = 'row_' . ++$count . (($count == 0) ? ' row_first' : '') . (($count >= ($limit - 1)) ? ' row_last' : '') . ((($count % 2) == 0) ? ' even' : ' odd');

                // Add the article image as enclosure
                $image = '';


                $objFile = \FilesModel::findByUuid($itemsObj->image);

                if ($objFile !== null)
                {
                    $image = $objFile->path;

                } else {
                    $settings = $this->getSettings($GLOBALS['AGAPE']['PROPERTIES']['ID']);

                    $objFile = \FilesModel::findByUuid($settings['category_no_image']);

                    if ($objFile !== null)
                    {
                        $image = $objFile->path;
                    }
                }

                //Products-Url
                $productsUrl = false;
                if($this->jumpTo)
                {
                    $objProductsPage = \PageModel::findByPk($this->jumpTo);
                    $productsUrl = ampersand( $this->generateFrontendUrl($objProductsPage->row(),'/'.$itemsObj->alias) );
                }

                $itemsArray[] = array(
                    'name' => $itemsObj->name,
                    'class' => $class,
                    'alias' => $itemsObj->alias,
                    'desription' => $itemsObj->desription,
                    'image' => $image,
                    'alt' => $itemsObj->alt,
                    'productsUrl' => $productsUrl
                );
            }
        }

        $this->Template->headline = $this->headline;
        $this->Template->items = $itemsArray;
        $this->Template->messages = ''; // Backwards compatibility
    }

}
