<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2016 Leo Feyer
 *
 * @license LGPL-3.0+
 */

namespace agape;


/**
 * Class ModuleAgape
 * @package agape
 */
abstract class ModuleAgape extends \Module
{

    /**
     * get current settings
     * @param $id
     * @return array|false|void
     */
    public function getSettings($id)
    {
        if(!$id) return;

        $dbObj = $this->Database->prepare('SELECT * FROM `tl_agape_properties` WHERE `id`=?')
            ->limit(1)
            ->execute($id);
        return $dbObj->fetchAssoc();

    }

}
