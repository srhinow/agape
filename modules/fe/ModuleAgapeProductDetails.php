<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    agape
 * @license	   LGPL +3.0
 * @filesource
 */


/**
 * Class ModuleAgapeProductDetails
 */
class ModuleAgapeProductDetails extends ModuleAgape
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'agape_product_details';


    /**
     * Target pages
     * @var array
     */
    protected $arrTargets = array();


    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### AGAPE PRODUCT DETAILS ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['product']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
        {
            \Input::setGet('product', \Input::get('auto_item'));
        }

        // Do not index or cache the page if no category item has been specified
        if (!\Input::get('product'))
        {
            global $objPage;
            $objPage->noSearch = 1;
            $objPage->cache = 0;
            return '';
        }

        // Fallback template
        if (strlen($this->fe_agape_template)) $this->strTemplate = $this->fe_agape_template;

        return parent::generate();
    }


    /**
     * Generate module
     */
    protected function compile()
    {
        /** @var \PageModel $objPage */
        global $objPage;

        $objProject = agape\AgapeProductsModel::findOneBy('alias',\Input::get('product'));

        if ($objProject === null)
        {
            // Do not index or cache the page
            $objPage->noSearch = 1;
            $objPage->cache = 0;

            // Send a 404 header
            header('HTTP/1.1 404 Not Found');
            $this->Template->messages = '<p class="error">' . sprintf($GLOBALS['TL_LANG']['MSC']['invalidPage'], \Input::get('items')) . '</p>';
            return;
        }

        $objFile = \FilesModel::findByUuid($objProject->image);

        if ($objFile !== null)
        {
            $image = $objFile->path;

        } else {
            $settings = $this->getSettings($GLOBALS['AGAPE']['PROPERTIES']['ID']);

            $objFile = \FilesModel::findByUuid($settings['category_no_image']);

            if ($objFile !== null)
            {
                $image = $objFile->path;
            }
        }

        $objCategory = agape\AgapeCategoriesModel::findOneBy('id',$objProject->pid);

        $itemArray = array(
            'id' => $objProject->id,
            'name' => $objProject->name,
            'alias' => $objProject->alias,
            'teaser' => $objProject->teaser,
            'text' => strlen($objProject->description)?$objProject->description:$objProject->teaser,
            'desription' => $objProject->description,
            'teaser' => $objProject->teaser,
            'designer' => $objProject->designer,
            'issued_at' => $objProject->issued_at,
            'image' => $image,
            'alt' => $objProject->alt
        );

        $this->Template->headline = $this->headline;
        $this->Template->item = $itemArray;

        //SEO-Werte setzen
        $objPage->pageTitle = (strlen($objProject->seo_title) > 0)? strip_tags(strip_insert_tags($objProject->seo_title)): $objCategory->name.' / '.$objProject->name;
        if(strlen($objProject->designer) > 0) $objPage->pageTitle .= ' ('.$objProject->designer.')';

        if(strlen($objProject->seo_keywords) > 0) $GLOBALS['TL_KEYWORDS'] .= strip_tags(strip_insert_tags($objProject->seo_keywords));
        $objPage->description = (strlen($objProject->seo_description) > 0)? strip_tags(strip_insert_tags($objProject->seo_description)): \Contao\StringUtil::substr($itemArray['text'],320);
    }

}
