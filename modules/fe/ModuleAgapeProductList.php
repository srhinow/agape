<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    agape
 * @license	   LGPL +3.0
 * @filesource
 */


/**
 * Class ModuleAgapeCategoryList
 */
class ModuleAgapeProductList extends ModuleAgape
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'agape_product_list';


    /**
     * Target pages
     * @var array
     */
    protected $arrTargets = array();


    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### AGAPE PRODUCT LIST ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['category']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
        {
            \Input::setGet('category', \Input::get('auto_item'));
        }

        // Do not index or cache the page if no category item has been specified
        if (!\Input::get('category'))
        {
            global $objPage;
            $objPage->noSearch = 1;
            $objPage->cache = 0;
            return '';
        }

        // Fallback template
        if (strlen($this->fe_agape_template)) $this->strTemplate = $this->fe_agape_template;

        return parent::generate();
    }


    /**
     * Generate module
     */
    protected function compile()
    {
        /** @var \PageModel $objPage */
        global $objPage;

        $categoryObj = agape\AgapeCategoriesModel::findByIdOrAlias(\Input::get('category'));

        if (null === $categoryObj)
        {
            /** @var \PageError404 $objHandler */
            $objHandler = new $GLOBALS['TL_PTY']['error_404']();
            $objHandler->generate($objPage->id);
        }

        $offset = 0;
        $limit = null;
        $itemsArray = array();

        // Maximum number of items
        if ($this->fe_iao_numberOfItems > 0)
        {
            $limit = $this->fe_iao_numberOfItems;
        }

        // Get the total number of items
        $intTotal = agape\AgapeProductsModel::countPublishedByPid($categoryObj->id);

        // Filter anwenden um die Gesamtanzahl zuermitteln
        if((int) $intTotal > 0)
        {
            $total = $intTotal - $offset;

            // Split the results
            if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage))
            {

                // Adjust the overall limit
                if (isset($limit))
                {
                    $total = min($limit, $total);
                }

                // Get the current page
                $id = 'page_n' . $this->id;
                $page = \Input::get($id) ?: 1;

                // Do not index or cache the page if the page number is outside the range
                if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
                {
                    global $objPage;
                    $objPage->noSearch = 1;
                    $objPage->cache = 0;

                    // Send a 404 header
                    header('HTTP/1.1 404 Not Found');
                    return;
                }

                // Set limit and offset
                $limit = $this->perPage;
                $offset = (max($page, 1) - 1) * $this->perPage;

                // Overall limit
                if ($offset + $limit > $total)
                {
                    $limit = $total - $offset;
                }

                // Add the pagination menu
                $objPagination = new Pagination($total, $this->perPage);
                $this->Template->pagination = $objPagination->generate("\n  ");
            }

            // Get the items
            if (isset($limit))
            {
                $itemsObj = agape\AgapeProductsModel::findPublishedByPid($categoryObj->id, $limit);
            }
            else
            {
                $itemsObj = agape\AgapeProductsModel::findPublishedByPid($categoryObj->id, 0);
            }

            $count = -1;

            while($itemsObj->next())
            {
                //row - Class
                $class = 'row_' . ++$count . (($count == 0) ? ' row_first' : '') . (($count >= ($limit - 1)) ? ' row_last' : '') . ((($count % 2) == 0) ? ' even' : ' odd');

                // Add the article image as enclosure
                $image = '';


                $objFile = \FilesModel::findByUuid($itemsObj->image);

                if ($objFile !== null)
                {
                    $image = $objFile->path;

                } else {
                    $settings = $this->getSettings($GLOBALS['AGAPE']['PROPERTIES']['ID']);

                    $objFile = \FilesModel::findByUuid($settings['category_no_image']);

                    if ($objFile !== null)
                    {
                        $image = $objFile->path;
                    }
                }

                //Product-Details-Url
                $detailsUrl = false;
                if($this->jumpTo)
                {
                    $objProductsPage = \PageModel::findByPk($this->jumpTo);
                    $detailsUrl = ampersand( $this->generateFrontendUrl($objProductsPage->row(),'/'.$itemsObj->alias) );
                }

                $itemsArray[] = array(
                    'name' => $itemsObj->name,
                    'class' => $class,
                    'alias' => $itemsObj->alias,
                    'teaser' => $itemsObj->teaser,
                    'desription' => $itemsObj->desription,
                    'designer' => $itemsObj->designer,
                    'issued_at' => $itemsObj->issued_at,
                    'image' => $image,
                    'alt' => $itemsObj->alt,
                    'detailsUrl' => $detailsUrl
                );
            }
        }

        $this->Template->headline = $this->headline;
        $this->Template->items = $itemsArray;
        $this->Template->messages = ''; // Backwards compatibility

        //SEO-Werte setzen
        if(strlen($categoryObj->seo_title) > 0) $objPage->pageTitle = strip_tags(strip_insert_tags($categoryObj->seo_title));
        if(strlen($categoryObj->seo_keywords) > 0) $GLOBALS['TL_KEYWORDS'] .= strip_tags(strip_insert_tags($categoryObj->seo_keywords));
        if(strlen($categoryObj->seo_description) > 0) $objPage->description = strip_tags(strip_insert_tags($categoryObj->seo_description));

    }

}
