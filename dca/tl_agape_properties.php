<?php

/**
 * PHP version > 5.6
 * @copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    agape
 * @license    LGPL
 * @filesource
 */

/**
 * Table tl_apage_properties
 */
$GLOBALS['TL_DCA']['tl_agape_properties'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'closed'                      => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
            )
        ),
        'onload_callback' => array
        (
            array('tl_agape_properties', 'create_property_entry')
        ),
    ),

    // Palettes
    'palettes' => array
    (
        'default'                     => '{api_properties},api_url,api_email,api_token;{import_images},category_api_images_folder,product_api_images_folder;{noimage_legend},category_no_image,product_no_image',

    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'modify' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'api_url' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_properties']['api_url'],
            'inputType'               => 'text',
            'default'		          => &$GLOBALS['AGAPE']['PROPERTIES']['API_URL'],
            'eval'                    => array( 'maxlength'=>255, 'tl_class'=>'clr long'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'api_email' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_properties']['api_email'],
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'email', 'maxlength'=>128, 'decodeEntities'=>true, 'tl_class'=>'clr w50'),
            'sql'                     => "varchar(128) NOT NULL default ''"
        ),
        'api_token' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_properties']['api_token'],
            'inputType'               => 'text',
            'eval'                    => array('decodeEntities'=>true, 'maxlength'=>128, 'tl_class'=>'w50'),
            'sql'                     => "varchar(128) NOT NULL default ''"
        ),
        'category_api_images_folder' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_properties']['category_api_images_folder'],
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array('multiple'=>false, 'fieldType'=>'radio'),
            'sql'                     => "blob NULL"
        ),
       'product_api_images_folder' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_properties']['product_api_images_folder'],
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array('multiple'=>false, 'fieldType'=>'radio'),
            'sql'                     => "blob NULL"
        ),
        'category_no_image' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_properties']['category_no_image'],
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array('filesOnly'=>true, 'extensions'=>Config::get('validImageTypes'), 'fieldType'=>'radio', 'mandatory'=>false),
            'sql'                     => "binary(16) NULL"
        ),
        'product_no_image' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_properties']['product_no_image'],
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array('filesOnly'=>true, 'extensions'=>Config::get('validImageTypes'), 'fieldType'=>'radio', 'mandatory'=>false),
            'sql'                     => "binary(16) NULL"
        ),
    )
);

/**
 * Class tl_apage_properties
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 * @copyright  Leo Feyer 2005-2012
 * @author     Leo Feyer <http://www.contao.org>
 * @package    Controller
 */
class tl_agape_properties extends \Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * create an entry if id=1 not exists
     */
    public function create_property_entry()
    {
        $testObj = $this->Database->execute('SELECT * FROM `tl_agape_properties`');

        if($testObj->numRows == 0)
        {
            $this->Database->prepare('INSERT INTO `tl_agape_properties`(`id`,`api_url`) VALUES( ?, ?)')
                           ->execute($GLOBALS['AGAPE']['PROPERTIES']['ID'], $GLOBALS['AGAPE']['PROPERTIES']['API_URL']);
        }
    }
}

