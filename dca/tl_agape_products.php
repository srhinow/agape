<?php

/**
 * PHP version > 5.6
 * @copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    agape
 * @license    LGPL
 * @filesource
 */


/**
 * Table tl_agape_products
 */
$GLOBALS['TL_DCA']['tl_agape_products'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'enableVersioning'            => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'pid' => 'index',
            )
        )
    ),
    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('pid'),
            'flag'					  => 1,
            'panelLayout'             => 'filter;sort,search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('name'),
            'format'                  => '%s',
            'label_callback'		  => array('tl_agape_products', 'listEntries')
        ),
        'global_operations' => array
        (
            'categories' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_agape_products']['categories'],
                'href'                => 'table=tl_agape_categories',
                'class'               => 'header_icon',
                'icon'  			  => $GLOBALS['AGAPE']['MODULE_RELPATH'].'/assets/icons/categories.png',
                'attributes'          => 'onclick="Backend.getScrollOffset();"'
            ),
            'synchronize' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_agape_products']['synchronize'],
                'href'                => 'key=synchronizeProducts',
                'icon'  			  => $GLOBALS['AGAPE']['MODULE_RELPATH'].'/assets/icons/refresh.png',
                'attributes'          => 'onclick="Backend.getScrollOffset();"'
            ),
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset();"',
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_agape_products']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'edit.gif',
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_agape_products']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.gif',
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_agape_products']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.gif',
                'attributes'          => 'onclick="if (!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\')) return false; Backend.getScrollOffset();"',
            ),
            'toggle' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_agape_products']['toggle'],
                'icon'                => 'visible.gif',
                'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback'     => array('tl_agape_products', 'toggleIcon')
            ),
//            'show' => array
//            (
//                'label'               => &$GLOBALS['TL_LANG']['tl_agape_products']['show'],
//                'href'                => 'act=show',
//                'icon'                => 'show.gif',
//            )
        )
    ),
    // Palettes
    'palettes' => array
    (
        '__selector__'                => array('addImage'),
        'default' => '{data_legend},name,alias,pid,teaser,description,designer,issued_at,addImage;{seo_legend},seo_title,seo_keywords,seo_description;{api_legend},api_id,api_product_category_id,api_issued_at,api_name,api_slug,api_summary,api_description,api_updated_at,api_designers_list;{extend_legend:hide},published'
    ),
    // Subpalettes
    'subpalettes' => array
    (
        'addImage'                    => 'image,alt',
//        'published'                   => 'start,stop'
    ),
    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['category'],
            'exclude'                 => true,
            'filter'                  => true,
            'sorting'                  => true,
            'inputType'               => 'select',
            'foreignKey'              => 'tl_agape_categories.name',
            'eval'                    => array('mandatory'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'modify' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'sorting' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
        ),
        'name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['name'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'alias' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['alias'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'alnum', 'doNotCopy'=>true, 'spaceToUnderscore'=>true, 'maxlength'=>128, 'tl_class'=>'w50'),
            'save_callback' => array
            (
                array('tl_agape_products', 'generateAlias')
            ),
            'sql'                     => "varchar(128) NOT NULL default ''"
        ),
        'teaser' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['teaser'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'textarea',
            'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr'),
            'sql'                     => "text NULL"
        ),
        'description' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['description'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'textarea',
            'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr'),
            'sql'                     => "text NULL"
        ),
        'designer' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['designer'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>255, 'tl_class'=>'long'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'issued_at' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['issued_at'],
            'inputType'               => 'text',
            'sql'                     => "int(5) unsigned NOT NULL"
        ),
        'addImage' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['addImage'],
            'exclude'                 => true,
            'filter'                  => true,
            'sorting'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('submitOnChange'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'image' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['image'],
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr'),
            'sql'                     => "binary(16) NULL"
        ),
        'alt' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['alt'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>255, 'tl_class'=>'long'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'published' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['published'],
            'filter'                  => true,
            'sorting'                 => true,
            'inputType'               => 'checkbox',
            'flag'                    => 11,
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'seo_title' => array(
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['seo_title'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'search'                  => true,
            'eval'                    => array('decodeEntities'=>true, 'maxlength'=>255,'tl_class'=>'long clr'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'seo_keywords' => array(
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['seo_keywords'],
            'exclude'                 => true,
            'inputType'               => 'textarea',
            'search'                  => true,
            'eval'                    => array('style'=>'height:60px', 'decodeEntities'=>true),
            'sql'                     => "text NULL"
        ),
        'seo_description' => array(
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['seo_description'],
            'exclude'                 => true,
            'inputType'               => 'textarea',
            'search'                  => true,
            'eval'                    => array('style'=>'height:60px', 'decodeEntities'=>true, 'maxlength'=>180,'tl_class'=>'clr'),
            'sql'                     => "varchar(180) NOT NULL default ''"
        ),
        'api_id' => array
        (
//            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['api_id'],
            'inputType'               => 'text',
            'eval'                    => array('readonly'=>true),
            'sql'                     => "int(10) unsigned NOT NULL"
        ),
        'api_product_category_id' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['api_product_category_id'],
            'inputType'               => 'text',
            'eval'                    => array('readonly'=>true),
            'sql'                     => "int(10) unsigned NOT NULL"
        ),
        'api_issued_at' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['api_issued_at'],
            'inputType'               => 'text',
            'eval'                    => array('readonly'=>true),
            'sql'                     => "int(5) unsigned NOT NULL"
        ),
        'api_name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['api_name'],
            'inputType'               => 'text',
            'eval'                    => array('readonly'=>true),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'api_slug' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['api_slug'],
            'inputType'               => 'text',
            'eval'                    => array('readonly'=>true),
            'sql'                     => "varchar(128) NOT NULL default ''"
        ),
        'api_summary' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['api_summary'],
            'inputType'               => 'textarea',
            'eval'                    => array('readonly'=>true),
            'sql'                     => "text NULL"
        ),
       'api_description' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['api_description'],
            'inputType'               => 'textarea',
            'eval'                    => array('readonly'=>true),
            'sql'                     => "text NULL"
        ),
        'api_updated_at' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['api_updated_at'],
            'inputType'               => 'text',
            'eval'                    => array('readonly'=>true),
            'sql'                     => "datetime NULL"
        ),
        'api_designers_list' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_agape_products']['api_designers_list'],
            'inputType'               => 'text',
            'eval'                    => array('readonly'=>true),
            'sql'                     => "varchar(128) NOT NULL default ''"
        ),
    )
);


/**
 * Class tl_agape_products
 */
class tl_agape_products extends Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Auto-generate an alias if it has not been set yet
     * @param $varValue
     * @param DataContainer $dc
     * @return string
     * @throws Exception
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate an alias if there is none
        if ($varValue == '')
        {
            $autoAlias = true;
            $varValue = standardize(\StringUtil::restoreBasicEntities($dc->activeRecord->name));
        }

        $objAlias = $this->Database->prepare("SELECT id FROM tl_agape_products WHERE id=? OR alias=?")
            ->execute($dc->id, $varValue);

        // Check whether the page alias exists
        if ($objAlias->numRows > 1)
        {
            if (!$autoAlias)
            {
                throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-' . $dc->id;
        }

        return $varValue;
    }

    /**
     * Return the link picker wizard
     *
     * @param DataContainer $dc
     *
     * @return string
     */
    public function pagePicker(DataContainer $dc)
    {
        return ' <a href="' . (($dc->value == '' || strpos($dc->value, '{{link_url::') !== false) ? 'contao/page.php' : 'contao/file.php') . '?do=' . Input::get('do') . '&amp;table=' . $dc->table . '&amp;field=' . $dc->field . '&amp;value=' . rawurlencode(str_replace(array('{{link_url::', '}}'), '', $dc->value)) . '&amp;switch=1' . '" title="' . specialchars($GLOBALS['TL_LANG']['MSC']['pagepicker']) . '" onclick="Backend.getScrollOffset();Backend.openModalSelector({\'width\':768,\'title\':\'' . specialchars(str_replace("'", "\\'", $GLOBALS['TL_DCA'][$dc->table]['fields'][$dc->field]['label'][0])) . '\',\'url\':this.href,\'id\':\'' . $dc->field . '\',\'tag\':\'ctrl_'. $dc->field . ((Input::get('act') == 'editAll') ? '_' . $dc->id : '') . '\',\'self\':this});return false">' . Image::getHtml('pickpage.gif', $GLOBALS['TL_LANG']['MSC']['pagepicker'], 'style="vertical-align:top;cursor:pointer"') . '</a>';
    }

    public function convertImagePath()
    {
        $dwlObj = $this->Database->prepare('SELECT * FROM `tl_agape_products`')->execute();

        if($dwlObj->numRows > 0)
        {
            while($dwlObj->next())
            {
                if($dwlObj->file !== NULL) continue;
                $strFile = $dwlObj->dl_url;
                $objFile = \FilesModel::findByPath($strFile);

                // Existing file is being replaced (see #4818)
                if ($objFile !== null)
                {

                    $objFile->tstamp = time();
                    $objFile->path   = $strFile;
                    $objFile->hash   = md5_file(TL_ROOT . '/' . $strFile);
                    $objFile->save();

                    $strUuid = $objFile->uuid;
                }
                else
                {
                    $strUuid = \Dbafs::addResource($strFile)->uuid;
                }

                $set = array('file' => $strUuid);
                $this->Database->prepare('UPDATE `tl_agape_products` %s WHERE `id`=?')->set($set)->execute($dwlObj->id);
            }
        }
    }

    /**
     * modify list-view
     * @param array
     * @param string
     * @return string
     */
    public function listEntries($row, $label)
    {
        // exit();
        if ($row['image'] != '')
        {
            $objModel = \FilesModel::findByUuid($row['image']);
            $thumbUrl = \Image::get($objModel->path,100,100);

            if ($objModel === null)
            {
                // print_r($label);
                $label = $row['name'];
            }
            else
            {
                $label =  sprintf('<img src="%s" alt="" title="%s" style="float:left;"> <div style="vertical-align:middle; display:inline-block; margin-left:30px; margin-top: 40px; font-size: 14px;">%s</div>',$thumbUrl, $row['name'],$row['name']);
            }
        }

        return sprintf('<div style="float:left">%s</div>',$label) . "\n";
    }

    /**
     * Return the "toggle visibility" button
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (strlen(Input::get('tid')))
        {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->hasAccess('tl_agape_products::published', 'alexf'))
        {
            return '';
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published'])
        {
            $icon = 'invisible.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"').'</a> ';
    }


    /**
     * Disable/enable a user group
     *
     * @param integer       $intId
     * @param boolean       $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        if ($dc)
        {
            $dc->id = $intId; // see #8043
        }

        $objVersions = new Versions('tl_agape_products', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_agape_products']['fields']['published']['save_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_agape_products']['fields']['published']['save_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, ($dc ?: $this));
                }
                elseif (is_callable($callback))
                {
                    $blnVisible = $callback($blnVisible, ($dc ?: $this));
                }
            }
        }

        // Update the database
        $this->Database->prepare("UPDATE tl_agape_products SET tstamp=". time() .", published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);

        $objVersions->create();

    }
}
