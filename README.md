Contao-Erweiterung für die Verwaltung von agape-Produkten
--

Im Contao-Backend lassen sich die Categorien und Produkte von agape verwalten. Dazu können die Produkte über eine agape-api-Schnittstelle mit den Moduleignenen Datenbanktabellen syncronisiert werden. So muss nicht bei jedem Seitenaufruf die API-Schnitstelle abgefragt werden und auch die Überabreitung und gezielte Anzeige von bestimmten Produkten und Kategorien ist so möglich.

Die zugrundeliegende API-Dokumentation befindet sich im Modul-Ordner unter assets/doku/AgapeAPI-v1_0-Documentation.pdf.