<?php
/**
 * Created by agape_contao.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 06.09.17
 */

/**
 * Class AgapeApi
 */
class AgapeApi extends \Backend
{
    protected $url = '';
    protected $email = '';
    protected $token = '';
    protected $locale = 'en';

    protected $category = array();
    protected $categories = array();

    protected $product = array();
    protected $products = array();

    protected $imagePathType = ''; // category or product
    protected $categoryImagesDir = '';
    protected $productImagesDir = '';

        protected function setProperties() {
        $this->import('Database');

        $resultObj = $this->Database->prepare('SELECT * FROM `tl_agape_properties` WHERE id=?')
            ->limit(1)
            ->execute($GLOBALS['AGAPE']['PROPERTIES']['ID']);

        if($resultObj->numRows > 0) {
            $this->url = $resultObj->api_url;
            $this->email = $resultObj->api_email;
            $this->token = $resultObj->api_token;
            $this->categoryImagesDir = $resultObj->category_api_images_folder;
            $this->productImagesDir = $resultObj->product_api_images_folder;
        }
    }

    protected function getAllCategories() {
        $this->setProperties();

        $urlString = $this->url.'/'.$this->locale.'/product_categories?user_email='.$this->email.'&user_token='.$this->token;
        $result = file_get_contents($urlString);

        $this->categories = json_decode($result, true);

    }

    protected function getCategory($categoryId) {
        $this->setProperties();

        if( (int) $categoryId < 1) return array();

        $urlString = $this->url.'/'.$this->locale.'/product_category/'.$categoryId.'?user_email='.$this->email.'&user_token='.$this->token;
        $result = file_get_contents($urlString);

        $this->category = json_decode($result, true);
    }

    protected function getProductsByCategory($categoryId) {
        $this->setProperties();

        if( (int) $categoryId < 1) return array();

        $urlString = $this->url.'/'.$this->locale.'/product_category/'.$categoryId.'/products?user_email='.$this->email.'&user_token='.$this->token;
        $result = file_get_contents($urlString);

        $this->products = json_decode($result, true);
    }

    protected function getProduct($productId) {
        $this->setProperties();

        if( (int) $productId < 1) return array();

        $urlString = $this->url.'/'.$this->locale.'/product/'.$productId.'?user_email='.$this->email.'&user_token='.$this->token;
        $result = file_get_contents($urlString);

        $this->product = json_decode($result, true);
    }

    /**
     * synchronize the Categories with API-Categories
     */
    public function synchronizeCategories()
    {
        $this->getAllCategories();

        if(is_array($this->categories) && count($this->categories) > 0)
        {

            foreach($this->categories as $category)
            {
                $this->imagePathType = 'category';

                $addImage = $singleImgUuid = '';

                // Fotos
                if(is_array($category['category_photos']) && count($category['category_photos']) > 0)
                {
                    $api_category_photos = serialize($category['category_photos']);
                    $addImage = 1;
                    $imgForSave = 0;

                    foreach($category['category_photos'] as $key => $photoData)
                    {
                        if($key == $imgForSave) $singleImgUuid = $this->getImagesAndSaveLocal($photoData['url'], $category,true);
                        else $this->getImagesAndSaveLocal($photoData['url'], $category);
                    }

                } else {
                    $api_category_photos = '';
                }

                //testen ob DB UPDATE oder INSERT
                $existObj = $this->Database->prepare("SELECT api_id FROM `tl_agape_categories` WHERE api_id=?")->execute($category['id']);

                //Update
                if($existObj->numRows > 0)
                {
                    //Daten fuer DB setzen
                    $set = array(
                        'tstamp' => time(),
                        'modify' => time(),
                        'image' => $singleImgUuid,
                        'api_slug' => $category['slug'],
                        'api_name' => $category['name'],
                        'api_description' => $category['description'],
                        'api_updated_at' => date('Y-m-d H:i:s',strtotime($category['updated_at'])),
                        'api_category_photos' => $api_category_photos,
                    );

                    $this->Database->prepare("UPDATE tl_agape_categories %s WHERE api_id=?")->set($set)->execute($category['id']);
                }
                //Insert
                else {
                    $set = array(
                        'tstamp' => time(),
                        'modify' => strtotime($category['updated_at']),
                        'name' => $category['name'],
                        'alias' => $category['slug'],
                        'description' => $category['description'],
                        'addImage' => $addImage,
                        'image' => $singleImgUuid,
                        'api_id' => $category['id'],
                        'api_slug' => $category['slug'],
                        'api_name' => $category['name'],
                        'api_description' => $category['description'],
                        'api_updated_at' => date('Y-m-d H:i:s',strtotime($category['updated_at'])),
                        'api_category_photos' => $api_category_photos
                    );

                    $this->Database->prepare("INSERT INTO tl_agape_categories %s")->set($set)->execute();
                }
            }

            $_SESSION['TL_ERROR'] = '';
            $_SESSION['TL_CONFIRM'][] = $GLOBALS['TL_LANG']['tl_agape_categories']['success_categories_imported'];
            setcookie('BE_PAGE_OFFSET', 0, 0, '/');
            $this->redirect(str_replace('&key=synchronizeCategories', '', $this->Environment->request));

        } else {

            $_SESSION['TL_ERROR'][] = $GLOBALS['TL_LANG']['tl_agape_categories']['error_no_categories_imported'];
            $_SESSION['TL_CONFIRM'] = '';
            setcookie('BE_PAGE_OFFSET', 0, 0, '/');
            $this->redirect(str_replace('&key=synchronizeCategories', '', $this->Environment->request));
        }
    }
    /**
     * synchronize the Products with API-Products
     */
    public function synchronizeProducts()
    {
        //hole alle Kategorien
        $categoriesObj = $this->Database->prepare('SELECT * FROM tl_agape_categories')->execute();

        if($categoriesObj->numRows > 0) {

            //Daten als Array
            $this->categories = $categoriesObj->fetchAllAssoc();

            // abrechen wenn es kein befuelltes Array ist
            if(!is_array($this->categories) && count($this->categories) < 1) return;

            //Kategorien durchlaufen
            foreach($this->categories as $category) {

                // erzeugt das $this->products Array
                $this->getProductsByCategory($category['api_id']);

                //gehe noch eine ebene tiefer in das Produkt-Array
                $productsArr = $this->products[0]['products'];

                if(!is_array($productsArr) && count($productsArr) < 1) continue;

                //Produkte durchlaufen
                foreach($productsArr as $product) {

                    //Variablen definieren
                    $singleImgUuid = $addImage = '';

                    // Fotos
                    if(is_array($product['product_variants']) && count($product['product_variants']) > 0)
                    {
                        //Varianten durchlaufen um die Fotos zu extrahieren
                        foreach($product['product_variants'] as $variant) {

                            if(is_array($variant['variant_photos']) && count($variant['variant_photos']) > 0)
                            {
                                $addImage = 1;
                                $imgForSave = 0;

                                //Variant-Fotos durchlaufen
                                foreach ($variant['variant_photos'] as $key => $photoData) {
                                    if ($key == $imgForSave) $singleImgUuid = $this->getImagesAndSaveLocal($photoData['url'], $product, true);
                                    else $this->getImagesAndSaveLocal($photoData['url'], $product);


                                }
                            }
                        }
                    }

                    //testen ob DB UPDATE oder INSERT
                    $existObj = $this->Database->prepare("SELECT api_id FROM `tl_agape_products` WHERE api_id=?")->execute($product['id']);


                    //Update
                    if($existObj->numRows > 0)
                    {
                        //Daten fuer DB setzen
                        $set = array(
                           # 'alias' => $this->getNoDuplicateProductAlias($product),
                            'tstamp' => time(),
                            'modify' => time(),
                           # 'addImage' => $addImage,
                           # 'image' => $singleImgUuid,
                           # 'designer' => $product['designers_list'],
                            'api_product_category_id' => $product['product_category_id'],
                            'api_issued_at' => (int) $product['issued_at'],
                            'api_name' => $product['name'],
                            'api_slug' => $product['slug'],
                            'api_summary' => $product['summary'],
                            'api_description' => $product['description'],
                            'api_updated_at' => date('Y-m-d H:i:s',strtotime($product['updated_at'])),
                            'api_designers_list' => $product['designers_list'],
                        );

                        $this->Database->prepare("UPDATE tl_agape_products %s WHERE api_id=?")->set($set)->execute($product['id']);
                    }
                    //Insert
                    else {

                        $set = array(
                            'tstamp' => time(),
                            'pid' => $this->getCategoryIdFromApiCategoryId($product['product_category_id']),
                            'modify' => strtotime($product['updated_at']),
                            'name' => $product['name'],
                            'alias' => $this->getNoDuplicateProductAlias($product),
                            'teaser' => $product['summary'],
                            'description' => $product['description'],
                            'designer' => $product['designers_list'],
                            'addImage' => $addImage,
                            'image' => $singleImgUuid,
                            'api_id' => $product['id'],
                            'api_product_category_id' => $product['product_category_id'],
                            'api_issued_at' => (int) $product['issued_at'],
                            'api_name' => $product['name'],
                            'api_slug' => $product['slug'],
                            'api_summary' => $product['summary'],
                            'api_description' => $product['description'],
                            'api_updated_at' => date('Y-m-d H:i:s',strtotime($product['updated_at'])),
                            'api_designers_list' => $product['designers_list'],
                        );

                        $this->Database->prepare("INSERT INTO tl_agape_products %s")->set($set)->execute();
                    }

                }
            }

            $_SESSION['TL_ERROR'] = '';
            $_SESSION['TL_CONFIRM'][] = $GLOBALS['TL_LANG']['tl_agape_products']['success_products_imported'];
            setcookie('BE_PAGE_OFFSET', 0, 0, '/');
            $this->redirect(str_replace('&key=synchronizeProducts', '', $this->Environment->request));

        } else {

            $_SESSION['TL_ERROR'][] = $GLOBALS['TL_LANG']['tl_agape_products']['error_no_categories_exist'];
            $_SESSION['TL_CONFIRM'] = '';
            setcookie('BE_PAGE_OFFSET', 0, 0, '/');
            $this->redirect(str_replace('&key=synchronizeProducts', '', $this->Environment->request));
        }
    }
    /**
     * @param string $srcPath
     * @param array $data
     * @param bool $return
     * @return string
     */
    protected function getImagesAndSaveLocal($srcPath='', $data=array(), $return=false)
    {
//        print_r($srcPath); exit();

        if(strlen($srcPath))
        {
            $path_parts = pathinfo($srcPath);
            $destPathUuId = ($this->imagePathType == 'category') ? $this->categoryImagesDir : $this->productImagesDir;
            $destPathObj = \FilesModel::findById($destPathUuId);
            $destPath = $destPathObj->path.'/'.$data['slug'];
            $targetImage = $destPath.'/'.$path_parts['basename'];

            if(!file_exists(TL_ROOT.'/'.$targetImage)) {

                //Ordner rekursiv erstellen lassen wenn er noch nicht existiert
                new \Folder($destPath);

                if (strlen($srcPath) > 0 && !file_exists(TL_ROOT . '/' . $targetImage)) {

                    //kleineres Bild holen
                    $srcPath = str_replace('upload/', 'upload/h_1000,w_1000,c_fit/', $srcPath);

                    $imgContent = file_get_contents($srcPath);

                    $localImg = new \File($targetImage);
                    $localImg->write($imgContent);
                    $localImg->close();

                }
            }

            if($return) {
                return \FilesModel::findByPath($targetImage)->uuid;
            }
        }
    }

    protected function getCategoryIdFromApiCategoryId($id) {

        if((int)$id < 1) return;

        $catObj = $this->Database->prepare('SELECT id FROM tl_agape_categories WHERE api_id=?')
            ->limit(1)
            ->execute($id);

        return (int) $catObj->id;
    }

    /**
     * gibt ein individuelles Alias-String zurück wenn bereits ein gleiches Alias in der Datenbank exisitert.
     * @param array $product
     * @return string
     */
    protected function getNoDuplicateProductAlias($product = array())
    {
        $newAlias = $product['slug'];

        $productObj = agape\AgapeProductsModel::findAnotherProductByIdAndAlias($product['slug'],$product['id']);

        if($productObj != null)
        {
            $categoryObj = agape\AgapeCategoriesModel::findByIdOrAlias($this->getCategoryIdFromApiCategoryId($product['product_category_id']));

            $newAlias = ($categoryObj == null) ? $product['slug'].'-'.$product['id']: $categoryObj->alias.'-'.$product['slug'];
        }

        return $newAlias;
    }
}