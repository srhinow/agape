<?php
/**
 * @copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    agape
 * @license    http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 * @filesource
 */

class AgapeHooks extends \Frontend
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * replace agape-specific inserttag if get-paramter isset
     * agape::category|product::colname from objPage
     * @param string $strTag
     * @return string
     */
    public function iaoReplaceInsertTags($strTag)
    {
        if (substr($strTag,0,7) == 'agape::')
        {
//            global $objPage;

            $split = explode('::',$strTag);

            switch($split[1]){
                case 'category':
                    $categoryId = \Input::get('category');
                    $objCategory = agape\AgapeCategoriesModel::findCategoryByIdOrAlias($categoryId);
                    if($objCategory != null) return $objCategory->$split[2];

                    break;
                case 'product':
                    $productId = \Input::get('product');
                    $objProduct = agape\AgapeProductsModel::findByIdOrAlias($productId);
                    if($objProduct != null) return $objProduct->$split[2];
            }

        }

        return false;
    }

    /**
     * passt die Breadcrumb-Navi den Category -> Produktliste -> Produktdetails mit den jeweiligen Namen an
     * @param $arrItems
     * @param $objModule
     * @return mixed
     */
    public function agapeGenerateBreadcrumb($arrItems, $objModule)
    {

        $count = count($arrItems) -1;
        if(\Input::get('category')) {
            $objCategory = agape\AgapeCategoriesModel::findOneBy('alias',\Input::get('category'));
            if($objCategory != null) {
                $arrItems[$count]['title'] = $objCategory->name;
            }
        }
        if(\Input::get('product')) {

            $objProduct = agape\AgapeProductsModel::findOneBy('alias',\Input::get('product'));
            if($objProduct != null) {
                //Produktbereich anpassen
                $arrItems[$count]['title'] = $objProduct->name;

                //Kategorielink anpassen
                $catCount = $count -1;
                $catPageId = $arrItems[$catCount]['data']['id'];

                $objCategory = agape\AgapeCategoriesModel::findById($objProduct->pid);
                $pageObj = \PageModel::findById($catPageId);

                if($pageObj != null && $objCategory != null) {
                    $newPageUrl = $pageObj->getFrontendUrl(((\Config::get('useAutoItem') && !\Config::get('disableAlias')) ? '/' : '/items/') . ((!\Config::get('disableAlias') && $objCategory->alias != '') ? $objCategory->alias : $objCategory->id));
                    $arrItems[$catCount]['href'] = $newPageUrl;
                    $arrItems[$catCount]['title'] = $objCategory->name;
                    $arrItems[$catCount]['link'] = $objCategory->name;
                }
            }
        }

        return $arrItems;
    }
}
