<?php
/**
 * Created by agape_contao.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 06.09.17
 */

$GLOBALS['AGAPE']['MODULE_RELPATH'] = "system/modules/agape";
$GLOBALS['AGAPE']['PROPERTIES']['ID'] = 1;
$GLOBALS['AGAPE']['PROPERTIES']['API_URL'] = "http://dev.agapedesign.it/api/v1";

/**
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['BE_MOD'], 1, array(
        'agape' => array
        (
            'agape_products' => array (
                'tables' => array('tl_agape_products','tl_agape_categories'),
                'icon'  => $GLOBALS['AGAPE']['MODULE_RELPATH'].'/assets/icons/shop.png',
                'synchronizeCategories'=> array('AgapeApi', 'synchronizeCategories'),
                'synchronizeProducts'=> array('AgapeApi', 'synchronizeProducts'),
            ),
            'agape_properties' => array (
                'tables' => array('tl_agape_properties'),
                'callback' => 'ModuleApageProperties',
                'icon'  => $GLOBALS['AGAPE']['MODULE_RELPATH'].'/assets/icons/process.png',
            )
        )
    )
);

/**
 * Frontend modules
 */
$GLOBALS['FE_MOD']['agape_fe'] = array
(
    'fe_agape_categories' => 'ModuleAgapeCategoryList',
    'fe_agape_products' => 'ModuleAgapeProductList',
    'fe_agape_product_details' => 'ModuleAgapeProductDetails',
);

/** Hooks */
$GLOBALS['TL_HOOKS']['generateBreadcrumb'][] = array('AgapeHooks', 'agapeGenerateBreadcrumb');