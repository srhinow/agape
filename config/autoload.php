<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2017 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'agape',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'AgapeApi'                   => 'system/modules/agape/classes/AgapeApi.php',
	'AgapeHooks'                 => 'system/modules/agape/classes/AgapeHooks.php',

	// Models
	'agape\AgapeCategoriesModel' => 'system/modules/agape/models/AgapeCategoriesModel.php',
	'agape\AgapeProductsModel'   => 'system/modules/agape/models/AgapeProductsModel.php',

	// Modules
	'ModuleApageProperties'      => 'system/modules/agape/modules/be/ModuleApageProperties.php',
	'ModuleAgapeProductList'     => 'system/modules/agape/modules/fe/ModuleAgapeProductList.php',
	'agape\ModuleAgape'          => 'system/modules/agape/modules/fe/ModuleAgape.php',
	'ModuleAgapeCategoryList'    => 'system/modules/agape/modules/fe/ModuleAgapeCategoryList.php',
	'ModuleAgapeProductDetails'  => 'system/modules/agape/modules/fe/ModuleAgapeProductDetails.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'agape_product_list'    => 'system/modules/agape/templates',
	'agape_product_details' => 'system/modules/agape/templates',
	'agape_category_list'   => 'system/modules/agape/templates',
));
