<?php
/**
 * TL_ROOT/system/modules/agape/languages/de/tl_agape_categories.php
 *
 * Contao extension: agape
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */

/** Legends */
$GLOBALS['TL_LANG']['tl_agape_categories']['data_legend'] = "Kategorie-Daten";
$GLOBALS['TL_LANG']['tl_agape_categories']['seo_legend'] = "Suchmaschinenoptimierung";
$GLOBALS['TL_LANG']['tl_agape_categories']['api_legend'] = "von API übermittelte Daten";
$GLOBALS['TL_LANG']['tl_agape_categories']['extend_legend'] = "weitere Einstellungen";

/** Fields */
$GLOBALS['TL_LANG']['tl_agape_categories']['name']['0'] = "Name";
$GLOBALS['TL_LANG']['tl_agape_categories']['name']['1'] = "Name der Kategorie.";
$GLOBALS['TL_LANG']['tl_agape_categories']['alias']['0'] = "Alias";
$GLOBALS['TL_LANG']['tl_agape_categories']['alias']['1'] = "Alias, um auf die Kategorie verweisen zu können.";
$GLOBALS['TL_LANG']['tl_agape_categories']['description']['0'] = "Beschreibung";
$GLOBALS['TL_LANG']['tl_agape_categories']['description']['1'] = "";
$GLOBALS['TL_LANG']['tl_agape_categories']['addImage']['0'] = "Bild hinzufügen";
$GLOBALS['TL_LANG']['tl_agape_categories']['addImage']['1'] = "";
$GLOBALS['TL_LANG']['tl_agape_categories']['image']['0'] = "Bild";
$GLOBALS['TL_LANG']['tl_agape_categories']['image']['1'] = "";
$GLOBALS['TL_LANG']['tl_agape_categories']['alt']['0'] = "Alternativtext für das Bild";
$GLOBALS['TL_LANG']['tl_agape_categories']['alt']['1'] = "wird im Quelltext geschrieben und hilft Brailgeräte und Google ;)";
$GLOBALS['TL_LANG']['tl_agape_categories']['sorting']['0'] = "Sortierung (Zahl)";
$GLOBALS['TL_LANG']['tl_agape_categories']['published']['0'] = "auf Website anzeigen";
//$GLOBALS['TL_LANG']['tl_agape_categories']['published']['1'] = "wird im Quelltext geschrieben und hilft Brailgeräte und Google ;)";

$GLOBALS['TL_LANG']['tl_agape_categories']['seo_title']['0'] = "SEO-Titel";
$GLOBALS['TL_LANG']['tl_agape_categories']['seo_title']['1'] = "Titel für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_agape_categories']['seo_keywords']['0'] = "SEO-Schlagwörte";
$GLOBALS['TL_LANG']['tl_agape_categories']['seo_keywords']['1'] = "Keywords für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_agape_categories']['seo_description']['0'] = "SEO-Beschreibungstext";
$GLOBALS['TL_LANG']['tl_agape_categories']['seo_description']['1'] = "Beschreibungstext für die Suchmaschienenoptimierung (Quelltext)";

/** Actions */
$GLOBALS['TL_LANG']['tl_agape_categories']['new']['0'] = "Neue Kategorie";
$GLOBALS['TL_LANG']['tl_agape_categories']['new']['1'] = "Neue Kategorie erstellen.";
$GLOBALS['TL_LANG']['tl_agape_categories']['show']['0'] = "Einzelheiten der Kategorie";
$GLOBALS['TL_LANG']['tl_agape_categories']['show']['1'] = "Einzelheiten der Kategorie ID %s anzeigen";
$GLOBALS['TL_LANG']['tl_agape_categories']['edit']['0'] = "Kategorie bearbeiten";
$GLOBALS['TL_LANG']['tl_agape_categories']['edit']['1'] = "Kategorie ID %s bearbeiten";
$GLOBALS['TL_LANG']['tl_agape_categories']['copy']['0'] = "Kategorie kopieren";
$GLOBALS['TL_LANG']['tl_agape_categories']['copy']['1'] = "Kategorie ID %s kopieren";
$GLOBALS['TL_LANG']['tl_agape_categories']['cut']['0'] = "Begriff verschieben";
$GLOBALS['TL_LANG']['tl_agape_categories']['cut']['1'] = "Begriff ID %s verschieben";
$GLOBALS['TL_LANG']['tl_agape_categories']['delete']['0'] = "Kategorie löschen";
$GLOBALS['TL_LANG']['tl_agape_categories']['delete']['1'] = "Kategorie ID %s löschen";
$GLOBALS['TL_LANG']['tl_agape_categories']['copyChildren']['0'] = "Begriff mit Unterbegriffen kopieren";
$GLOBALS['TL_LANG']['tl_agape_categories']['copyChildren']['1'] = "Begriff ID %s mit Unterbegriffen kopieren";
$GLOBALS['TL_LANG']['tl_agape_categories']['synchronize'] = "Kategorien synchronisieren";

/** Message */
$GLOBALS['TL_LANG']['tl_agape_categories']['success_categories_imported'] = 'Es wurden alle verfügbaren Kategorien geholt';
$GLOBALS['TL_LANG']['tl_agape_categories']['error_no_categories_imported'] = 'Es konnten keine Kategorien geholt werden.';