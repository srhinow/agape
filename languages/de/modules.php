<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2012 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['agape'] = array('agape', '');
$GLOBALS['TL_LANG']['MOD']['agape_products'] = array('Agape-Produkte', 'agape-Kategorien und dazugehörende Produkte verwalten.');
$GLOBALS['TL_LANG']['MOD']['agape_properties'] = array('Einstellungen', 'Dieses Modul erlaubt es Ihnen alle Einstellungen von agape zu verwalten.');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['BBK']      = 'Bilderbuchkino';
$GLOBALS['TL_LANG']['FMD']['bbk_reserv_calendar']    = array('BBK Reservierungs-Kalender', 'Fügt der Seite einen Kalender aller Bilderbuchkino-Reservierungen hinzu.');
$GLOBALS['TL_LANG']['FMD']['bbk_reserv_form']    = array('BBK Reservierungs-Formular', 'ein Formular für die Reservierung eines BBKs');
$GLOBALS['TL_LANG']['FMD']['bbk_details']    = array('BBK Detailansicht', 'eine Detailansicht für die Reservierung eines BBKs');
$GLOBALS['TL_LANG']['FMD']['bbk_list']    = array('BBK Listansicht', 'eine Listansicht für die Reservierung eines BBKs');
$GLOBALS['TL_LANG']['FMD']['bbk_pdf_create_button']    = array('BBK PDF-Erstellen-Button', 'eine PDF von den verfügbaren BBKs erstellen.');
