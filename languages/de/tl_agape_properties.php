<?php
/**
 * TL_ROOT/system/modules/agape/languages/de/tl_agape_properties.php
 *
 * Contao extension: agape
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */


/** Legends */
$GLOBALS['TL_LANG']['tl_agape_properties']['api_properties'] = "Produkt-Daten";
$GLOBALS['TL_LANG']['tl_agape_properties']['import_images'] = "Import-Einstellungen";
$GLOBALS['TL_LANG']['tl_agape_properties']['noimage_legend'] = "Kein-Bild-Einstellungen";

/* Fields */
$GLOBALS['TL_LANG']['tl_agape_properties']['api_url']['0'] = "API-URL";
$GLOBALS['TL_LANG']['tl_agape_properties']['api_url']['1'] = "ohne abschließenden Schrägstrich (/), Standart: http://dev.agapedesign.it/api/v1";
$GLOBALS['TL_LANG']['tl_agape_properties']['api_email']['0'] = "API-User-Email";
$GLOBALS['TL_LANG']['tl_agape_properties']['api_email']['1'] = "zur der Schnittstelle erlaubte Emailadresse. Muss mit dem agape-Support geklährt werden.";
$GLOBALS['TL_LANG']['tl_agape_properties']['api_token']['0'] = "API-Token";
$GLOBALS['TL_LANG']['tl_agape_properties']['api_token']['1'] = "zur der Schnittstelle und der User-Emailadresse zugewiesene Token-String. Muss mit dem agape-Support geklährt werden.";
$GLOBALS['TL_LANG']['tl_agape_properties']['category_api_images_folder']['0'] = "Ordner für die Kategoriebilder";
$GLOBALS['TL_LANG']['tl_agape_properties']['category_api_images_folder']['1'] = "Weisen Sie hier den Ort zu, in dem die Kategorie-Bilder durch die Syncronisation abgelegt werden.";
$GLOBALS['TL_LANG']['tl_agape_properties']['product_api_images_folder']['0'] = "Ordner für die Produktbilder";
$GLOBALS['TL_LANG']['tl_agape_properties']['product_api_images_folder']['1'] = "Weisen Sie hier den Ort zu, in dem die Produkt-Bilder durch die Syncronisation abgelegt werden.";
$GLOBALS['TL_LANG']['tl_agape_properties']['category_no_image']['0'] = "kein-Bild (Kategorie)";
$GLOBALS['TL_LANG']['tl_agape_properties']['category_no_image']['1'] = "Weisen Sie hier ein Bild zu, welches angezeigt wird, wenn es kein Bild zu der Kategorie gibt.";
$GLOBALS['TL_LANG']['tl_agape_properties']['product_no_image']['0'] = "kein-Bild (Produkt)";
$GLOBALS['TL_LANG']['tl_agape_properties']['product_no_image']['1'] = "Weisen Sie hier ein Bild zu, welches angezeigt wird, wenn es kein Bild zu dem Produkt gibt.";

