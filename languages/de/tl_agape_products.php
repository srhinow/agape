<?php
/**
 * TL_ROOT/system/modules/agape/languages/de/tl_agape_products.php
 *
 * Contao extension: agape
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */

$GLOBALS['TL_LANG']['tl_agape_products']['title'] = "Produkte";
$GLOBALS['TL_LANG']['tl_agape_products']['seo_legend'] = "Suchmaschinenoptimierung";
$GLOBALS['TL_LANG']['tl_agape_products']['categories'] = "Kategorien";
$GLOBALS['TL_LANG']['tl_agape_products']['synchronize'] = "Produkte synchronisieren";

/** Legends */
$GLOBALS['TL_LANG']['tl_agape_products']['data_legend'] = "Produkt-Daten";
$GLOBALS['TL_LANG']['tl_agape_products']['api_legend'] = "von API übermittelte Daten";
$GLOBALS['TL_LANG']['tl_agape_products']['extend_legend'] = "weitere Einstellungen";

/* Fields */
$GLOBALS['TL_LANG']['tl_agape_products']['name']['0'] = "Name";
$GLOBALS['TL_LANG']['tl_agape_products']['name']['1'] = "Name des Produkts.";
$GLOBALS['TL_LANG']['tl_agape_products']['alias']['0'] = "Alias";
$GLOBALS['TL_LANG']['tl_agape_products']['alias']['1'] = "Alias, um auf den Produkt verweisen zu können.";
$GLOBALS['TL_LANG']['tl_agape_products']['category']['0'] = "Kategorie";
$GLOBALS['TL_LANG']['tl_agape_products']['category']['1'] = "die zugehoerige Kategorie auswählen.";
$GLOBALS['TL_LANG']['tl_agape_products']['addImage']['0'] = "Bild hinzufügen";
$GLOBALS['TL_LANG']['tl_agape_products']['addImage']['1'] = "";
$GLOBALS['TL_LANG']['tl_agape_products']['image']['0'] = "Bild";
$GLOBALS['TL_LANG']['tl_agape_products']['image']['1'] = "Bild auswählen, welches zum Produkt bereit gestellt weden soll.";
$GLOBALS['TL_LANG']['tl_agape_products']['alt']['0'] = "Alternativtext für das Bild";
$GLOBALS['TL_LANG']['tl_agape_products']['alt']['1'] = "wird im Quelltext geschrieben und hilft Brailgeräte und Google ;)";
$GLOBALS['TL_LANG']['tl_agape_products']['teaser']['0'] = "Kurzbeschreibung";
$GLOBALS['TL_LANG']['tl_agape_products']['teaser']['1'] = "Teaser zum Produkt.";
$GLOBALS['TL_LANG']['tl_agape_products']['description']['0'] = "Beschreibung";
$GLOBALS['TL_LANG']['tl_agape_products']['description']['1'] = "Beschreibung zum Produkt.";
$GLOBALS['TL_LANG']['tl_agape_products']['published']['0'] = "online sichtbar";
$GLOBALS['TL_LANG']['tl_agape_products']['published']['1'] = "Haken setzen wenn dieser Eintrag auf der Website angezeigt werden soll.";
$GLOBALS['TL_LANG']['tl_agape_products']['designer']['0'] = "Designer";
$GLOBALS['TL_LANG']['tl_agape_products']['designer']['1'] = "";
$GLOBALS['TL_LANG']['tl_agape_products']['issued_at']['0'] = "erstellt im Jahr";

$GLOBALS['TL_LANG']['tl_agape_products']['seo_title']['0'] = "SEO-Titel";
$GLOBALS['TL_LANG']['tl_agape_products']['seo_title']['1'] = "Titel für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_agape_products']['seo_keywords']['0'] = "SEO-Schlagwörte";
$GLOBALS['TL_LANG']['tl_agape_products']['seo_keywords']['1'] = "Keywords für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_agape_products']['seo_description']['0'] = "SEO-Beschreibungstext";
$GLOBALS['TL_LANG']['tl_agape_products']['seo_description']['1'] = "Beschreibungstext für die Suchmaschienenoptimierung (Quelltext)";

/* Activities */
$GLOBALS['TL_LANG']['tl_agape_products']['new']['0'] = "Neues Produkt";
$GLOBALS['TL_LANG']['tl_agape_products']['new']['1'] = "Neuen Produkt erstellen.";
$GLOBALS['TL_LANG']['tl_agape_products']['show']['0'] = "Einzelheiten vom Produkt";
$GLOBALS['TL_LANG']['tl_agape_products']['show']['1'] = "Einzelheiten vom Produkt ID %s anzeigen";
$GLOBALS['TL_LANG']['tl_agape_products']['edit']['0'] = "Produkt bearbeiten";
$GLOBALS['TL_LANG']['tl_agape_products']['edit']['1'] = "Produkt ID %s bearbeiten";
$GLOBALS['TL_LANG']['tl_agape_products']['copy']['0'] = "Produkt kopieren";
$GLOBALS['TL_LANG']['tl_agape_products']['copy']['1'] = "Produkt ID %s kopieren";
$GLOBALS['TL_LANG']['tl_agape_products']['cut']['0'] = "Produkt verschieben";
$GLOBALS['TL_LANG']['tl_agape_products']['cut']['1'] = "Produkt ID %s verschieben";
$GLOBALS['TL_LANG']['tl_agape_products']['delete']['0'] = "Produkt löschen";
$GLOBALS['TL_LANG']['tl_agape_products']['delete']['1'] = "Produkt ID %s löschen";
$GLOBALS['TL_LANG']['tl_agape_products']['toggle']['0'] = "aktivieren/ deaktivieren";
$GLOBALS['TL_LANG']['tl_agape_products']['toggle']['1'] = "Den Eintrag aktivieren bzw. deaktivieren";

/** Message */
$GLOBALS['TL_LANG']['tl_agape_products']['success_products_imported'] = 'Es wurden alle verfügbaren Produkte geholt';
$GLOBALS['TL_LANG']['tl_agape_products']['error_no_categories_exist'] = 'Es sind keine Kategorien für den import von Produkten vorhanden. Bitte importieren sie zuerst die Kategorien.';